/******************************************************************************
 * @brief    系统配置
 *
 * Copyright (c) 2020  <morro_luo@163.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs: 
 * Date           Author       Notes 
 * 2020-09-21     Morro        Initial version
 ******************************************************************************/
#include "config.h"

/* 项目名称 */
#define PROJECT          "RIL-Demo"
#define SWVER            "V1.01"

#pragma message(PROJECT""SWVER" <"__DATE__" "__TIME__">")
